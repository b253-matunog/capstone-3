import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";

export default function Products () {

  const [ products, setProducts ] = useState([]);

  useEffect(() => {
    fetch(`http://localhost:4000/api/products/`)
      .then(res => res.json())
      .then(data => {
        
        setProducts(data.map((product) => {
          return (
            <ProductCard key={product._id} product={product} />
          )
        }))
      })
  }, []);

  return (
    <div className="row justify-content-between">
      <h2 className="mt-4 mb-3 text-center">Our Products</h2>
      { products }    
    </div>
  )

}