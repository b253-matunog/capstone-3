import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home () {
  return (
    <>
      <Banner isValidRoute={true}/>
      <h1 className="mt-5 mb-4 text-center">Product Categories</h1>
      <Highlights/>
    </>
  )
}