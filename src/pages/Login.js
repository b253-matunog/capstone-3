import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from "sweetalert2";

import UserContext from '../UserContext';

export default function Login () {

  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [isActive, setIsActive] = useState(false);

  function loginUser (e) {
    // Prevents page redirection via form submission
    e.preventDefault();

    fetch("http://localhost:4000/api/users/login", 
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      }
    )
      .then(res => res.json())
      .then(data => {

        if(typeof data.access !== "undefined") {

          localStorage.setItem("token", data.access);
          retrieveUserDetails(data.access);

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to my Ecomerce App"
          })

        } else {

          Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Check your login credentials and try again!"
          })

        }
      })

  }

  const retrieveUserDetails = (token) => {
    fetch("http://localhost:4000/api/users/userDetails", {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.json())
      .then(data => {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      });
  };

  useEffect(() => {

    // Validation to enable submit button when all fields are populated and both passwords match
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (

    (user.id !== null) ?
      <Navigate to="/products"/>
    :
      <Row className="login justify-content-center align-items-center">
        <Col className="d-flex justify-content-center align-items-center">
          <Card className="px-md-4">
            <Card.Body>
              <Form onSubmit={(e) => loginUser(e)}>
                <h1 className="text-center">Login</h1>
                <Form.Group className="mb-3" controlId="userEmail">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control 
                    type="email"
                    placeholder="Enter email" 
                    value={ email } 
                    onChange={e => setEmail(e.target.value)} 
                    required/>
                  <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                  </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="password">
                  <Form.Label>Password</Form.Label>
                  <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={ password } 
                    onChange={e => setPassword(e.target.value)} 
                    required/>
                </Form.Group>

                {
                  isActive ? 
                  <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                  </Button> : 
                  <Button variant="danger" type="submit" id="submitBtn" disabled>
                  Submit
                </Button>
                }
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>

  )

}