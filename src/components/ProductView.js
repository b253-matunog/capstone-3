import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {

	// The "useParams" hook allows us to retrieve the courseId passed via the URL
	const { productId } = useParams();

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [ name, setName ] = useState("");
	const [ description, setDescription ] = useState("");
	const [ price , setPrice ] = useState(0);
	const [ quantity, setQuantity ] = useState(1);

	const decrementCounter = () => {
		if (quantity > 1) {
			setQuantity(quantity - 1);
		}
	};

	const incrementCounter = () => {
		setQuantity(quantity + 1);
	};

	const checkOut = (productId) => {
		fetch(`http://localhost:4000/api/users/checkout`, {
			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				productName: name,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data !== "") {
				Swal.fire({
					title: "Order Successfully Checked Out",
					icon: 'success',
					text: "You have successfully checked out your order."
				});

				// The navigate hook will allow us to navigate and redirect the products
				navigate("/products");

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		});
	}

	useEffect(() => {
		fetch(`http://localhost:4000/api/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [ productId ]);

	return (
		<Container className="mt-5">
			<Row className="justify-content-center align-items-center mx-lg-5">
				<Col className="mx-lg-5">
					<Card className="my-3 mx-lg-5">
					    <Card.Body>
					        <Card.Title className="text-center mb-3">{ name }</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{ description }</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>Php { price }</Card.Text>
									<Card.Text className="mb-0">Quantity</Card.Text>
					        <div className="mb-3"> 
					        	<Button variant="secondary" onClick={decrementCounter}>-</Button>
								<span className="mx-2">{quantity}</span>
								<Button variant="secondary" onClick={incrementCounter}>+</Button>
							</div>
					        { user.id !== null ? 
					        		<div className='d-flex justify-content-end'>
												<Button variant="primary" onClick={() => checkOut(productId)}>Check Out</Button>
											</div>
					        	:  
					        		<div className="d-flex justify-content-end">
												<Link className="btn btn-danger btn-block" to="/login">Log in to Check Out Order</Link>
											</div>
					        }
					    </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)

}