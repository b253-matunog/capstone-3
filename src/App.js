// Built-in imports
import { useState } from "react";

// downloaded package imports  
import { Container } from "react-bootstrap";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";

import './App.css'
// (user-defined) components imports (alphabetical or according file structure)
import AppNavBar from "./components/AppNavBar";
import Products from "./pages/Products";
import ProductView from "./components/ProductView";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";
import Error from "./pages/Error";
import { UserProvider } from "./UserContext";

function App() {

  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null
  });

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  return (

    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/products" element={<Products/>}/>
            <Route path="/products/:productId" element={<ProductView/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="*" element={<Error />}/>
          </Routes>
        </Container>
      </Router>
    </UserProvider>

  )
}

export default App;
